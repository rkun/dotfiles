#!/bin/sh


# install zplug
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh

# install dein.vim
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
chmod +x installer.sh
./installer.sh ~/.cache/dein
rm installer.sh

ln -sf `pwd`/nvim/init.vim ~/.config/nvim/

ln -sf `pwd`/zsh/dot_zshrc ~/.zshrc

